﻿using ServiceAuto_tema1.DAL;

namespace ServiceAuto_tema1.RepositoryL.Interfaces
{
	interface IAppointmentRepository
	{
		AppointmentDAO getAppointmentDAO();
	}
}
