﻿using System;
using System.Text;
using System.Security.Cryptography;
using ServiceAuto_tema1.RepositoryL.Interfaces;
using ServiceAuto_tema1.Entities;

namespace ServiceAuto_tema1.BL
{
	class UserService
	{
		IUserRepository _repository;

		public UserService(IUserRepository repository)
		{
			_repository = repository;
		}

		//// ----- UserService METHODS ----- ////

		/**
		 *  returns encrypted input (password)
		 */
		static string getMd5Hash(string input)
		{
			// Create a new instance of the MD5CryptoServiceProvider object.
			MD5 md5Hasher = MD5.Create();

			// Convert the input string to a byte array and compute the hash.
			byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

			// Create a new Stringbuilder to collect the bytes
			// and create a string.
			StringBuilder sBuilder = new StringBuilder();

			// Loop through each byte of the hashed data 
			// and format each one as a hexadecimal string.
			for (int i = 0; i < data.Length; i++)
			{
				sBuilder.Append(data[i].ToString("x2"));
			}

			// Return the hexadecimal string.
			return sBuilder.ToString();
		}

		/**
		 *  returns User fetched from db by username and password
		 */
		public User getUser(String username, String password)
		{
			return _repository.getUserDAO().getUser(username, getMd5Hash(password));
		}

		/**
		 *  inserts serviceAgent User into db
		 *  returns true if succeded
		 */
		public bool createServiceAgent(String username, String password, String name)
		{
			return _repository.getUserDAO().createServiceAgent(username, getMd5Hash(password), name);
		}
	
	}
}
