﻿using System;

namespace ServiceAuto_tema1.Entities
{
	class Appointment
	{
		private String _client;
		private String _phone;
		private String _date;
		private String _time;
		private String _car;
		private String _problem;
		private String _status;

		public Appointment()
		{
		}

		public Appointment(String client)
		{
			Client = client;
		}

		public Appointment(String client, String phone, String date, String time, String car, String problem, String status)
		{
			Client = client;
			Phone = phone;
			Date = date;
			Time = time;
			Car = car;
			Problem = problem;
			Status = status;
		}

		public string Client
		{
			get
			{
				return _client;
			}

			set
			{
				_client = value;
			}
		}

		public string Phone
		{
			get
			{
				return _phone;
			}

			set
			{
				_phone = value;
			}
		}

		public string Date
		{
			get
			{
				return _date;
			}

			set
			{
				_date = value;
			}
		}

		public string Time
		{
			get
			{
				return _time;
			}

			set
			{
				_time = value;
			}
		}

		public string Car
		{
			get
			{
				return _car;
			}

			set
			{
				_car = value;
			}
		}

		public string Problem
		{
			get
			{
				return _problem;
			}

			set
			{
				_problem = value;
			}
		}

		public string Status
		{
			get
			{
				return _status;
			}

			set
			{
				_status = value;
			}
		}
	}
}
