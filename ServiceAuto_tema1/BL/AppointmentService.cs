﻿using System;
using System.Collections.Generic;
using ServiceAuto_tema1.RepositoryL.Interfaces;
using ServiceAuto_tema1.Entities;

namespace ServiceAuto_tema1.BL
{
	class AppointmentService
	{
		IAppointmentRepository _repository;

		public AppointmentService(IAppointmentRepository repository)
		{
			_repository = repository;
		}

		//// ----- AppointmentService METHODS ----- ////

		/**
		 * fetches all Client's Appointments
		 */
		public List<Appointment> getClientAppointments(String client)
		{
			return _repository.getAppointmentDAO().getClientAppointments(client);
		}

		/**
		 * fetches all Appointments between 2 dates
		 */
		public List<Appointment> getAppointmentsBetween2Dates(String date1, String date2)
		{
			return _repository.getAppointmentDAO().getAppointmentsBetween2Dates(date1,date2);
		}

		/**
		 * fetches all Appointments in one day
		 */
		public List<Appointment> getAppointmentsForOneDay(String date)
		{
			return _repository.getAppointmentDAO().getAppointmentsForOneDay(date);
		}

		/**
		 * checkes Date and Time availability 
		 * returns true if they are available for an Appointmenty
		 */
		public bool getAppointmentDateTimeAvailable(String date, String time)
		{
			return _repository.getAppointmentDAO().getAppointmentDateTimeAvailable(date, time);
		}

		/**
		 *  inserts an Appointment in db
		 * returns true the Appointment was inserted
		 */
		public bool makeAppointment(String client, String phone, String date, String time, String car, String problem, String status)
		{
			return _repository.getAppointmentDAO().makeAppointment(client, phone, date, time, car, problem, status);
		}

		/**
		*  updates Appointments status
		*  returns true if the status was updated
		*/
		public bool updateAppointment(String date, String time, String status)
		{
			return _repository.getAppointmentDAO().updateAppointment(date, time, status);
		}

	}
}
