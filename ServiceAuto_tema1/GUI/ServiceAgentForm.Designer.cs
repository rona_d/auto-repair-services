﻿namespace ServiceAuto_tema1.GUI
{
	partial class ServiceAgentForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.makeAnAppointmentButton = new System.Windows.Forms.Button();
			this.viewAppointmentsButton = new System.Windows.Forms.Button();
			this.buttonsPanel = new System.Windows.Forms.Panel();
			this.logOutButton = new System.Windows.Forms.Button();
			this.makeAppointmentPanel = new System.Windows.Forms.Panel();
			this.label9 = new System.Windows.Forms.Label();
			this.minComboBox = new System.Windows.Forms.ComboBox();
			this.hourComboBox = new System.Windows.Forms.ComboBox();
			this.dateMakeAppointmentTextBox = new System.Windows.Forms.TextBox();
			this.makeAppointmentButton = new System.Windows.Forms.Button();
			this.problemTextBox = new System.Windows.Forms.RichTextBox();
			this.carTextBox = new System.Windows.Forms.TextBox();
			this.phoneTextBox = new System.Windows.Forms.TextBox();
			this.clientTextBox = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.monthCalendarMakeAppointment = new System.Windows.Forms.MonthCalendar();
			this.label2 = new System.Windows.Forms.Label();
			this.viewOneDayAppointmentsPanel = new System.Windows.Forms.Panel();
			this.updateStatusAppointmentButton = new System.Windows.Forms.Button();
			this.dateOneDayTextBox = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.monthCalendarOneDay = new System.Windows.Forms.MonthCalendar();
			this.dataGridView2 = new System.Windows.Forms.DataGridView();
			this.TimeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ClientColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.CarColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ProblemColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.StatusColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.PhoneColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.label10 = new System.Windows.Forms.Label();
			this.buttonsPanel.SuspendLayout();
			this.makeAppointmentPanel.SuspendLayout();
			this.viewOneDayAppointmentsPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Minion Pro SmBd", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(12, 55);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(279, 27);
			this.label1.TabIndex = 0;
			this.label1.Text = "WELCOME, SERVICE AGENT!";
			// 
			// makeAnAppointmentButton
			// 
			this.makeAnAppointmentButton.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.makeAnAppointmentButton.Font = new System.Drawing.Font("Lucida Sans", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.makeAnAppointmentButton.Location = new System.Drawing.Point(3, 89);
			this.makeAnAppointmentButton.Name = "makeAnAppointmentButton";
			this.makeAnAppointmentButton.Size = new System.Drawing.Size(260, 82);
			this.makeAnAppointmentButton.TabIndex = 1;
			this.makeAnAppointmentButton.Text = "Make an appointment";
			this.makeAnAppointmentButton.UseVisualStyleBackColor = false;
			this.makeAnAppointmentButton.Click += new System.EventHandler(this.makeAnAppointmentButton_Click);
			// 
			// viewAppointmentsButton
			// 
			this.viewAppointmentsButton.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.viewAppointmentsButton.Font = new System.Drawing.Font("Lucida Sans", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.viewAppointmentsButton.Location = new System.Drawing.Point(3, 253);
			this.viewAppointmentsButton.Name = "viewAppointmentsButton";
			this.viewAppointmentsButton.Size = new System.Drawing.Size(260, 82);
			this.viewAppointmentsButton.TabIndex = 2;
			this.viewAppointmentsButton.Text = "View appointments in a day and update status";
			this.viewAppointmentsButton.UseVisualStyleBackColor = false;
			this.viewAppointmentsButton.Click += new System.EventHandler(this.viewAppointmentsButton_Click);
			// 
			// buttonsPanel
			// 
			this.buttonsPanel.Controls.Add(this.logOutButton);
			this.buttonsPanel.Controls.Add(this.makeAnAppointmentButton);
			this.buttonsPanel.Controls.Add(this.viewAppointmentsButton);
			this.buttonsPanel.Location = new System.Drawing.Point(12, 101);
			this.buttonsPanel.Name = "buttonsPanel";
			this.buttonsPanel.Size = new System.Drawing.Size(269, 505);
			this.buttonsPanel.TabIndex = 3;
			// 
			// logOutButton
			// 
			this.logOutButton.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.logOutButton.Font = new System.Drawing.Font("Minion Pro SmBd", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.logOutButton.Location = new System.Drawing.Point(0, 424);
			this.logOutButton.Name = "logOutButton";
			this.logOutButton.Size = new System.Drawing.Size(263, 33);
			this.logOutButton.TabIndex = 3;
			this.logOutButton.Text = "LogOut";
			this.logOutButton.UseVisualStyleBackColor = false;
			this.logOutButton.Click += new System.EventHandler(this.logOutButton_Click);
			// 
			// makeAppointmentPanel
			// 
			this.makeAppointmentPanel.Controls.Add(this.label10);
			this.makeAppointmentPanel.Controls.Add(this.label9);
			this.makeAppointmentPanel.Controls.Add(this.minComboBox);
			this.makeAppointmentPanel.Controls.Add(this.hourComboBox);
			this.makeAppointmentPanel.Controls.Add(this.dateMakeAppointmentTextBox);
			this.makeAppointmentPanel.Controls.Add(this.makeAppointmentButton);
			this.makeAppointmentPanel.Controls.Add(this.problemTextBox);
			this.makeAppointmentPanel.Controls.Add(this.carTextBox);
			this.makeAppointmentPanel.Controls.Add(this.phoneTextBox);
			this.makeAppointmentPanel.Controls.Add(this.clientTextBox);
			this.makeAppointmentPanel.Controls.Add(this.label7);
			this.makeAppointmentPanel.Controls.Add(this.label6);
			this.makeAppointmentPanel.Controls.Add(this.label5);
			this.makeAppointmentPanel.Controls.Add(this.label4);
			this.makeAppointmentPanel.Controls.Add(this.label3);
			this.makeAppointmentPanel.Controls.Add(this.monthCalendarMakeAppointment);
			this.makeAppointmentPanel.Controls.Add(this.label2);
			this.makeAppointmentPanel.Location = new System.Drawing.Point(299, 5);
			this.makeAppointmentPanel.Name = "makeAppointmentPanel";
			this.makeAppointmentPanel.Size = new System.Drawing.Size(1089, 698);
			this.makeAppointmentPanel.TabIndex = 4;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(309, 211);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(12, 17);
			this.label9.TabIndex = 16;
			this.label9.Text = ":";
			// 
			// minComboBox
			// 
			this.minComboBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.minComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.minComboBox.FormattingEnabled = true;
			this.minComboBox.Items.AddRange(new object[] {
            "00",
            "05",
            "10",
            "15",
            "20",
            "25",
            "30",
            "35",
            "40",
            "45",
            "50",
            "55"});
			this.minComboBox.Location = new System.Drawing.Point(327, 208);
			this.minComboBox.Name = "minComboBox";
			this.minComboBox.Size = new System.Drawing.Size(58, 24);
			this.minComboBox.TabIndex = 15;
			// 
			// hourComboBox
			// 
			this.hourComboBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.hourComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.hourComboBox.FormattingEnabled = true;
			this.hourComboBox.Items.AddRange(new object[] {
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23"});
			this.hourComboBox.Location = new System.Drawing.Point(249, 208);
			this.hourComboBox.Name = "hourComboBox";
			this.hourComboBox.Size = new System.Drawing.Size(54, 24);
			this.hourComboBox.TabIndex = 14;
			// 
			// dateMakeAppointmentTextBox
			// 
			this.dateMakeAppointmentTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.dateMakeAppointmentTextBox.Location = new System.Drawing.Point(240, 133);
			this.dateMakeAppointmentTextBox.Name = "dateMakeAppointmentTextBox";
			this.dateMakeAppointmentTextBox.ReadOnly = true;
			this.dateMakeAppointmentTextBox.Size = new System.Drawing.Size(239, 22);
			this.dateMakeAppointmentTextBox.TabIndex = 13;
			// 
			// makeAppointmentButton
			// 
			this.makeAppointmentButton.BackColor = System.Drawing.Color.GhostWhite;
			this.makeAppointmentButton.Font = new System.Drawing.Font("Minion Pro SmBd", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.makeAppointmentButton.Location = new System.Drawing.Point(72, 631);
			this.makeAppointmentButton.Name = "makeAppointmentButton";
			this.makeAppointmentButton.Size = new System.Drawing.Size(942, 43);
			this.makeAppointmentButton.TabIndex = 12;
			this.makeAppointmentButton.Text = "Make Appointment";
			this.makeAppointmentButton.UseVisualStyleBackColor = false;
			this.makeAppointmentButton.Click += new System.EventHandler(this.makeAppointmentButton_Click);
			// 
			// problemTextBox
			// 
			this.problemTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.problemTextBox.Location = new System.Drawing.Point(240, 465);
			this.problemTextBox.Name = "problemTextBox";
			this.problemTextBox.Size = new System.Drawing.Size(477, 125);
			this.problemTextBox.TabIndex = 11;
			this.problemTextBox.Text = "";
			// 
			// carTextBox
			// 
			this.carTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.carTextBox.Location = new System.Drawing.Point(240, 409);
			this.carTextBox.Name = "carTextBox";
			this.carTextBox.Size = new System.Drawing.Size(239, 22);
			this.carTextBox.TabIndex = 10;
			// 
			// phoneTextBox
			// 
			this.phoneTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.phoneTextBox.Location = new System.Drawing.Point(240, 347);
			this.phoneTextBox.Name = "phoneTextBox";
			this.phoneTextBox.Size = new System.Drawing.Size(239, 22);
			this.phoneTextBox.TabIndex = 9;
			// 
			// clientTextBox
			// 
			this.clientTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.clientTextBox.Location = new System.Drawing.Point(240, 280);
			this.clientTextBox.Name = "clientTextBox";
			this.clientTextBox.Size = new System.Drawing.Size(239, 22);
			this.clientTextBox.TabIndex = 8;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Lucida Sans", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(151, 520);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(78, 19);
			this.label7.TabIndex = 7;
			this.label7.Text = "Problem";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Lucida Sans", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(151, 412);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(39, 19);
			this.label6.TabIndex = 6;
			this.label6.Text = "Car";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Lucida Sans", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(151, 352);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(60, 19);
			this.label5.TabIndex = 5;
			this.label5.Text = "Phone";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Lucida Sans", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(151, 280);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(59, 19);
			this.label4.TabIndex = 4;
			this.label4.Text = "Client";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Lucida Sans", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(151, 215);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(50, 19);
			this.label3.TabIndex = 3;
			this.label3.Text = "Time";
			// 
			// monthCalendarMakeAppointment
			// 
			this.monthCalendarMakeAppointment.Location = new System.Drawing.Point(558, 50);
			this.monthCalendarMakeAppointment.MaxSelectionCount = 1;
			this.monthCalendarMakeAppointment.Name = "monthCalendarMakeAppointment";
			this.monthCalendarMakeAppointment.TabIndex = 1;
			this.monthCalendarMakeAppointment.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendarMakeAppointment_DateChanged);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Lucida Sans", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(151, 138);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(51, 19);
			this.label2.TabIndex = 0;
			this.label2.Text = "Date";
			// 
			// viewOneDayAppointmentsPanel
			// 
			this.viewOneDayAppointmentsPanel.Controls.Add(this.updateStatusAppointmentButton);
			this.viewOneDayAppointmentsPanel.Controls.Add(this.dateOneDayTextBox);
			this.viewOneDayAppointmentsPanel.Controls.Add(this.label8);
			this.viewOneDayAppointmentsPanel.Controls.Add(this.monthCalendarOneDay);
			this.viewOneDayAppointmentsPanel.Controls.Add(this.dataGridView2);
			this.viewOneDayAppointmentsPanel.Location = new System.Drawing.Point(299, 12);
			this.viewOneDayAppointmentsPanel.Name = "viewOneDayAppointmentsPanel";
			this.viewOneDayAppointmentsPanel.Size = new System.Drawing.Size(1089, 691);
			this.viewOneDayAppointmentsPanel.TabIndex = 5;
			// 
			// updateStatusAppointmentButton
			// 
			this.updateStatusAppointmentButton.BackColor = System.Drawing.Color.GhostWhite;
			this.updateStatusAppointmentButton.Font = new System.Drawing.Font("Minion Pro SmBd", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.updateStatusAppointmentButton.Location = new System.Drawing.Point(154, 600);
			this.updateStatusAppointmentButton.Name = "updateStatusAppointmentButton";
			this.updateStatusAppointmentButton.Size = new System.Drawing.Size(851, 47);
			this.updateStatusAppointmentButton.TabIndex = 20;
			this.updateStatusAppointmentButton.Text = "Update Status Appointment";
			this.updateStatusAppointmentButton.UseVisualStyleBackColor = false;
			this.updateStatusAppointmentButton.Click += new System.EventHandler(this.updateStatusAppointmentButton_Click);
			// 
			// dateOneDayTextBox
			// 
			this.dateOneDayTextBox.Location = new System.Drawing.Point(192, 240);
			this.dateOneDayTextBox.Name = "dateOneDayTextBox";
			this.dateOneDayTextBox.Size = new System.Drawing.Size(179, 22);
			this.dateOneDayTextBox.TabIndex = 19;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Lucida Sans", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.Location = new System.Drawing.Point(54, 245);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(111, 17);
			this.label8.TabIndex = 18;
			this.label8.Text = "Select a date";
			// 
			// monthCalendarOneDay
			// 
			this.monthCalendarOneDay.Location = new System.Drawing.Point(91, 18);
			this.monthCalendarOneDay.MaxSelectionCount = 1;
			this.monthCalendarOneDay.Name = "monthCalendarOneDay";
			this.monthCalendarOneDay.TabIndex = 17;
			this.monthCalendarOneDay.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendarOneDay_DateChanged);
			// 
			// dataGridView2
			// 
			this.dataGridView2.AllowUserToAddRows = false;
			this.dataGridView2.AllowUserToDeleteRows = false;
			this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
			this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView2.Location = new System.Drawing.Point(0, 283);
			this.dataGridView2.Name = "dataGridView2";
			this.dataGridView2.RowTemplate.Height = 24;
			this.dataGridView2.Size = new System.Drawing.Size(1089, 263);
			this.dataGridView2.TabIndex = 15;
			this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick);
			// 
			// TimeColumn
			// 
			this.TimeColumn.HeaderText = "Time";
			this.TimeColumn.Name = "TimeColumn";
			// 
			// ClientColumn
			// 
			this.ClientColumn.HeaderText = "Client";
			this.ClientColumn.Name = "ClientColumn";
			// 
			// CarColumn
			// 
			this.CarColumn.HeaderText = "Car";
			this.CarColumn.Name = "CarColumn";
			// 
			// ProblemColumn
			// 
			this.ProblemColumn.HeaderText = "Problem";
			this.ProblemColumn.Name = "ProblemColumn";
			// 
			// StatusColumn
			// 
			this.StatusColumn.HeaderText = "Status";
			this.StatusColumn.Name = "StatusColumn";
			// 
			// PhoneColumn
			// 
			this.PhoneColumn.HeaderText = "Phone";
			this.PhoneColumn.Name = "PhoneColumn";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Font = new System.Drawing.Font("Minion Pro SmBd", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label10.Location = new System.Drawing.Point(52, 50);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(338, 27);
			this.label10.TabIndex = 6;
			this.label10.Text = "For the new appointment, please select:";
			// 
			// ServiceAgentForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.LightBlue;
			this.ClientSize = new System.Drawing.Size(1400, 715);
			this.Controls.Add(this.buttonsPanel);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.viewOneDayAppointmentsPanel);
			this.Controls.Add(this.makeAppointmentPanel);
			this.Name = "ServiceAgentForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "ServiceAgentForm";
			this.buttonsPanel.ResumeLayout(false);
			this.makeAppointmentPanel.ResumeLayout(false);
			this.makeAppointmentPanel.PerformLayout();
			this.viewOneDayAppointmentsPanel.ResumeLayout(false);
			this.viewOneDayAppointmentsPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button makeAnAppointmentButton;
		private System.Windows.Forms.Button viewAppointmentsButton;
		private System.Windows.Forms.Panel buttonsPanel;
		private System.Windows.Forms.Panel makeAppointmentPanel;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button makeAppointmentButton;
		private System.Windows.Forms.RichTextBox problemTextBox;
		private System.Windows.Forms.TextBox carTextBox;
		private System.Windows.Forms.TextBox phoneTextBox;
		private System.Windows.Forms.TextBox clientTextBox;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.MonthCalendar monthCalendarMakeAppointment;
		private System.Windows.Forms.Panel viewOneDayAppointmentsPanel;
		private System.Windows.Forms.DataGridView dataGridView2;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.MonthCalendar monthCalendarOneDay;
		private System.Windows.Forms.DataGridViewTextBoxColumn TimeColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn ClientColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn CarColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn ProblemColumn;
		private System.Windows.Forms.DataGridViewCheckBoxColumn StatusColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn PhoneColumn;
		private System.Windows.Forms.Button logOutButton;
		private System.Windows.Forms.TextBox dateOneDayTextBox;
		private System.Windows.Forms.TextBox dateMakeAppointmentTextBox;
		private System.Windows.Forms.Button updateStatusAppointmentButton;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.ComboBox minComboBox;
		private System.Windows.Forms.ComboBox hourComboBox;
		private System.Windows.Forms.Label label10;
	}
}