﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using ServiceAuto_tema1.Entities;

namespace ServiceAuto_tema1.DAL
{
	class AppointmentDAO
	{
		private static AppointmentDAO _appointmentDAO = null;
		private String _connectionString = @"Data Source=DESKTOP-5T9GP8C\SQLEXPRESS;Initial Catalog=ps;Trusted_Connection=Yes;";
		SqlConnection _conn = null;

		private AppointmentDAO()
		{
			try
			{
				_conn = new SqlConnection(_connectionString);
			}
			catch (SqlException e)
			{
				//de facut ceva error handling, afisat mesaj, etc..
				_conn = null;
			}
		}

		//singleton 
		public static AppointmentDAO getInstance()
		{
			if (_appointmentDAO == null)
			{
				_appointmentDAO = new AppointmentDAO();
			}
			return _appointmentDAO;
		}

		//// ----- AppointmentDAO METHODS ----- ////

		/**
		 * fetches all Client's Appointments
		 */
		public List<Appointment> getClientAppointments(String client)
		{
			List<Appointment> a = new List<Appointment>();
			String sql = "SELECT * FROM appointment WHERE client='" + client + "'";
			try
			{
				_conn.Open();
				SqlCommand cmd = new SqlCommand(sql, _conn);
				SqlDataReader reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					a.Add(new Appointment(reader["client"].ToString(), reader["phone"].ToString(), String.Format("{0:dd-MM-yyyy}", reader["date"]), reader["time"].ToString(), reader["car"].ToString(), reader["problem"].ToString(), reader["status"].ToString()));
				}
				_conn.Close();

			}
			catch (SqlException e)
			{
				Console.WriteLine(e.Message);
				_conn.Close();
				return null;
			}
			return a;
		}

		/**
		 * fetches all Appointments between 2 dates
		 */
		public List<Appointment> getAppointmentsBetween2Dates(String date1,String date2)
		{
			List<Appointment> a = new List<Appointment>();
			String sql = "SELECT * FROM appointment WHERE date BETWEEN '" + date1 + "' AND '" + date2 + "'"; 
			try
			{
				_conn.Open();
				SqlCommand cmd = new SqlCommand(sql, _conn);
				SqlDataReader reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					a.Add(new Appointment(reader["client"].ToString(), reader["phone"].ToString(), String.Format("{0:dd-MM-yyyy}", reader["date"]), reader["time"].ToString(), reader["car"].ToString(), reader["problem"].ToString(), reader["status"].ToString()));
				}
				_conn.Close();

			}
			catch (SqlException e)
			{
				Console.WriteLine(e.Message);
				_conn.Close();
				return null;
			}
			return a;
		}

		/**
		 * fetches all Appointments in one day
		 */
		public List<Appointment> getAppointmentsForOneDay(String date)
		{
			List<Appointment> a = new List<Appointment>();
			String sql = "SELECT * FROM appointment WHERE date='" + date + "'";
			try
			{
				_conn.Open();
				SqlCommand cmd = new SqlCommand(sql, _conn);
				SqlDataReader reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					a.Add(new Appointment(reader["client"].ToString(), reader["phone"].ToString(), String.Format("{0:dd-MM-yyyy}", reader["date"]), reader["time"].ToString(), reader["car"].ToString(), reader["problem"].ToString(), reader["status"].ToString()));
				}
				_conn.Close();

			}
			catch (SqlException e)
			{
				Console.WriteLine(e.Message);
				_conn.Close();
				return null;
			}
			return a;
		}
		
		/**
		 * checkes Date and Time availability 
		 * returns true if they are available for an Appointmenty
		 */
		public bool getAppointmentDateTimeAvailable(String date, String time)
		{
			Appointment a = new Appointment();
			bool available = true;
			String sql = "SELECT client FROM appointment WHERE date='" + date + "' AND time='" + time +"'";
			try
			{
				_conn.Open();
				SqlCommand cmd = new SqlCommand(sql, _conn);
				SqlDataReader reader = cmd.ExecuteReader();
				reader.Read();
				a= new Appointment(reader["client"].ToString());
				available = false;
				_conn.Close();

			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
				_conn.Close();
				available = true;
			}
			return available;
		}

		/**
		 *  inserts an Appointment in db
		 * returns true the Appointment was inserted
		 */
		public bool makeAppointment(String client, String phone, String date, String time, String car, String problem, String status)
		{
			String sql = "INSERT INTO appointment(client,phone,date,time,car,problem,status) VALUES('" + client + "','" + phone + "','" + date + "','" + time + "','" + car + "','" + problem + "','" + status + "')";
			bool success = false;

			try
			{
				_conn.Open();
				SqlCommand cmd = new SqlCommand(sql, _conn);
				SqlDataReader reader = cmd.ExecuteReader();
				success = true;
				_conn.Close();

			}
			catch (SqlException e)
			{
				Console.WriteLine(e.Message);
				_conn.Close();
				success = false;
			}
			return success;
		}

		/**
		 *  updates Appointments status
		 *  returns true if the status was updated
		 */
		public bool updateAppointment(String date, String time, String status)
		{
			String sql = "UPDATE appointment SET status ='" + status + "' WHERE date='" + date + "' AND time='" + time+ "'";
			bool success = false;
			try
			{
				_conn.Open();
				SqlCommand cmd = new SqlCommand(sql, _conn);
				SqlDataReader reader = cmd.ExecuteReader();
				success = true;
				_conn.Close();

			}
			catch (SqlException e)
			{
				Console.WriteLine(e.Message);
				_conn.Close();
				return false;
			}
			return success;
		}
	}
}
