﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using ServiceAuto_tema1.BL;
using ServiceAuto_tema1.Entities;

namespace ServiceAuto_tema1.GUI
{
	public partial class AdminForm : Form
	{
		UserService userService = Program.USERSERVICE;
		AppointmentService appointmentService = Program.APPOINTMENTSERVICE;

		public AdminForm()
		{
			InitializeComponent();
			viewAppointmentsBetween2DatesPanel.Visible = true;
			viewClientAppointmentsPanel.Visible = false;
			createServiceAgentPanel.Visible = false;
		}

		// METHOD TO PUT LIST INTO DATATABLE
		public DataTable ToDataTable<T>(IList<T> list)
		{
			PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
			DataTable table = new DataTable();
			for (int i = 0; i < props.Count; i++)
			{
				PropertyDescriptor prop = props[i];
				table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
			}
			object[] values = new object[props.Count];
			foreach (T item in list)
			{
				for (int i = 0; i < values.Length; i++)
					values[i] = props[i].GetValue(item) ?? DBNull.Value;
				table.Rows.Add(values);
			}
			return table;
		}

			// ---- BUTTONS ---- //
		// BUTTON 2 DATES SHOW PANEL
		private void ViewAll2DatesAppointmentsButton_Click(object sender, EventArgs e)
		{
			viewAppointmentsBetween2DatesPanel.Visible = true;
			viewClientAppointmentsPanel.Visible = false;
			createServiceAgentPanel.Visible = false;

		}

		// BUTTON CLIENT SHOW PANEL
		private void ViewClientAppointmentsButton_Click(object sender, EventArgs e)
		{
			viewAppointmentsBetween2DatesPanel.Visible = false;
			viewClientAppointmentsPanel.Visible = true;
			createServiceAgentPanel.Visible = false;
		}

		// BUTTON SHOW CREATE SERVICE AGENT PANEL
		private void CreateServiceAgentButton_Click(object sender, EventArgs e)
		{
			viewAppointmentsBetween2DatesPanel.Visible = false;
			viewClientAppointmentsPanel.Visible = false;
			createServiceAgentPanel.Visible = true;
		}

		// BUTTON LOG OUT
		private void logOutButton_Click(object sender, EventArgs e)
		{
			LoginForm.getInstance().Show();
			this.Hide();
		}

		// BUTTON CREATE SERVICE AGENT
		private void createButton_Click(object sender, EventArgs e)
		{
			if (userService.createServiceAgent(usernameTextBox.Text, passwordTextBox.Text, nameTextBox.Text))
			{
				MessageBox.Show("Success");
			}
			else
			{
				MessageBox.Show("Not created");
			}

			usernameTextBox.Clear();
			passwordTextBox.Clear();
			nameTextBox.Clear();
		}

		// BUTTON SHOW CLIENT APPOINTMENTS
		private void showClientAppointmentsButton_Click(object sender, EventArgs e)
		{
			if (clientNameTextBox.Text != "")
			{
				List<Appointment> appointmentList = appointmentService.getClientAppointments(clientNameTextBox.Text);

				DataTable table = ToDataTable(appointmentList);
				clientAppointmentsDataGrid.DataSource = table;
			}
			
		}

			// ---- CALENDARS ---- //
		// CALENDAR MONTH 1 DATE CHANGED action
		private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
		{
			date1TextBox.Text = monthCalendar1.SelectionStart.Date.ToString("yyyy-MM-dd");
			if (date1TextBox.Text != "" && date2TextBox.Text != "")
			{
				List<Appointment> appointmentList = appointmentService.getAppointmentsBetween2Dates(date1TextBox.Text, date2TextBox.Text);

				DataTable table = ToDataTable(appointmentList);
				dataGridView1.DataSource = table;
			}
		}

		// CALENDAR MONTH 1 DATE CHANGED action
		private void monthCalendar2_DateChanged(object sender, DateRangeEventArgs e)
		{
			date2TextBox.Text = monthCalendar2.SelectionStart.Date.ToString("yyyy-MM-dd");
			if (date1TextBox.Text != "" && date2TextBox.Text != "")
			{
				List<Appointment> appointmentList = appointmentService.getAppointmentsBetween2Dates(date1TextBox.Text, date2TextBox.Text);

				DataTable table = ToDataTable(appointmentList);
				dataGridView1.DataSource = table;
			}
		}

	}
}
