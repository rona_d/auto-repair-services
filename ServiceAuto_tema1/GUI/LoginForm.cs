﻿using System;
using System.Windows.Forms;
using ServiceAuto_tema1.BL;
using ServiceAuto_tema1.Entities;


namespace ServiceAuto_tema1.GUI
{
	public partial class LoginForm : Form
	{
		private static LoginForm _loginFrom = null;
		
		//singleton
		public static LoginForm getInstance()
		{
			if (_loginFrom == null)
			{
				_loginFrom = new LoginForm();
			}
			return _loginFrom;
		}

		private LoginForm()
		{
			InitializeComponent();
		}

			// ---- BUTTONS ---- //
		// BUTTON LOGIN
		private void loginButton_Click(object sender, EventArgs e)
		{
			UserService userService = Program.USERSERVICE;

			User user = userService.getUser(usernameTextBox.Text, passwordTextBox.Text);

			if (user != null)
			{
				usernameTextBox.Clear();
				passwordTextBox.Clear();

				if (user.Role == "serviceAgent")
				{
					ServiceAgentForm serviceAgentFrom = new ServiceAgentForm();
					serviceAgentFrom.Show();
					this.Hide();
				}
				else if (user.Role == "admin")
				{
					AdminForm adminForm = new AdminForm();
					adminForm.Show();
					this.Hide();
				}
			}
		}

		// BUTTON EXIT
		private void btnExit_Click(object sender, EventArgs e)
		{
			this.Close();  //”this” refers to the form
		}

	}
}
