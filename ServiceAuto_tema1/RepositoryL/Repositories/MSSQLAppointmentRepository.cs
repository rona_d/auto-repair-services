﻿using ServiceAuto_tema1.DAL;
using ServiceAuto_tema1.RepositoryL.Interfaces;

namespace ServiceAuto_tema1.RepositoryL.Repositories
{
	class MSSQLAppointmentRepository : IAppointmentRepository
	{
		public AppointmentDAO getAppointmentDAO()
		{
			return AppointmentDAO.getInstance();
		}
	}
}
