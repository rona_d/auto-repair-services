﻿using System;

namespace ServiceAuto_tema1.Entities
{
	class User
	{
		private String _username;
		private String _password;
		private String _name;
		private String _role;


		public User()
		{
		}

		public User(String username, String password, String name, String role)
		{
			Username = username;
			Password = password;
			Name = name;
			Role = role;
		}


		public string Username
		{
			get
			{
				return _username;
			}

			set
			{
				_username = value;
			}
		}

		public string Password
		{
			get
			{
				return _password;
			}

			set
			{
				_password = value;
			}
		}

		public string Name
		{
			get
			{
				return _name;
			}

			set
			{
				_name = value;
			}
		}

		public string Role
		{
			get
			{
				return _role;
			}

			set
			{
				_role = value;
			}
		}
	}
}
