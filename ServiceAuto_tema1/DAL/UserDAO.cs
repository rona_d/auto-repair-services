﻿using System;
using System.Data.SqlClient;
using ServiceAuto_tema1.Entities;

namespace ServiceAuto_tema1.DAL
{
	class UserDAO
	{
		private static UserDAO _userDAO = null;
		private String _connectionString = @"Data Source=DESKTOP-5T9GP8C\SQLEXPRESS;Initial Catalog=ps;Trusted_Connection=Yes;";
		SqlConnection _conn = null;

		private UserDAO()
		{
			try
			{
				_conn = new SqlConnection(_connectionString);
			}
			catch (SqlException e)
			{
				//de facut ceva error handling, afisat mesaj, etc..
				_conn = null;
			}
		}

		//singleton
		public static UserDAO getInstance()
		{
			if (_userDAO == null)
			{
				_userDAO = new UserDAO();
			}
			return _userDAO;
		}
		
		//// ----- UserDAO METHODS ----- ////

		/**
		 * fetches User from db by username and password
		 */
		public User getUser(String username, String password)
		{
			User u = null;
			String sql = "SELECT * FROM users WHERE username='" + username + "' AND password='" + password + "'";
			try
			{
						_conn.Open();
				SqlCommand cmd = new SqlCommand(sql, _conn);
				SqlDataReader reader = cmd.ExecuteReader();
				reader.Read();
				u = new User(reader["username"].ToString(), reader["password"].ToString(), reader["name"].ToString(), reader["role"].ToString());
				_conn.Close();

			}
			catch (SqlException e)
			{
				Console.WriteLine(e.Message);
				return null;
			}
			return u;
		}

		/**
		 *  inserts a serviceAgent User in users
		 *  returns true if serviceAgent was inserted
		 */
		public bool createServiceAgent(String username, String password, String name)
		{
			bool success = false;
			String role = "serviceAgent";
			String sql = "INSERT INTO users(username,password,name,role) VALUES('" + username + "','" + password + "','" + name + "','" + role + "')";
			try
			{
				_conn.Open();
				SqlCommand cmd = new SqlCommand(sql, _conn);
				SqlDataReader reader = cmd.ExecuteReader();
				success = true;
				_conn.Close();

			}
			catch (SqlException e)
			{
				Console.WriteLine(e.Message);
				return false;
			}
			return success;
		}
	}
}
