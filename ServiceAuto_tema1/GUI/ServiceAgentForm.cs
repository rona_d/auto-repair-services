﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using ServiceAuto_tema1.BL;
using ServiceAuto_tema1.Entities;

namespace ServiceAuto_tema1.GUI
{
	public partial class ServiceAgentForm : Form
	{
		AppointmentService appointmentService = Program.APPOINTMENTSERVICE;
		String _timeUpdate = "";
		String _dateUpdate = "";
		String _statusUpdate = "";

		public ServiceAgentForm()
		{
			InitializeComponent();
			viewOneDayAppointmentsPanel.Visible = false;
			makeAppointmentPanel.Visible = true;
		}

		// METHOD TO PUT LIST INTO DATATABLE
		public DataTable ToDataTable<T>(IList<T> list)
		{
			PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
			DataTable table = new DataTable();
			for (int i = 0; i < props.Count; i++)
			{
				PropertyDescriptor prop = props[i];
				table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
			}
			object[] values = new object[props.Count];
			foreach (T item in list)
			{
				for (int i = 0; i < values.Length; i++)
					values[i] = props[i].GetValue(item) ?? DBNull.Value;
				table.Rows.Add(values);
			}
			return table;
		}

			// ---- BUTTONS ---- //
	    // BUTTON LOGOUT
		private void logOutButton_Click(object sender, EventArgs e)
		{
			LoginForm.getInstance().Show();
			this.Hide();
		}

		// BUTTON VIEW APPOINTMENTS PANEL
		private void viewAppointmentsButton_Click(object sender, EventArgs e)
		{
			viewOneDayAppointmentsPanel.Visible = true;
			makeAppointmentPanel.Visible = false;
	
		}

		// BUTTON MAKE AN APPOINTMENT PANEL
		private void makeAnAppointmentButton_Click(object sender, EventArgs e)
		{
			viewOneDayAppointmentsPanel.Visible = false;
			makeAppointmentPanel.Visible = true;
		}

		// BUTTON MAKE AN APPOINTMENT 
		private void makeAppointmentButton_Click(object sender, EventArgs e)
		{
			String time = hourComboBox.Text + ":" + minComboBox.Text + ":00";
			String status = "unrepaired";
			if (dateMakeAppointmentTextBox.Text != "" && clientTextBox.Text != "" && phoneTextBox.Text != "" && carTextBox.Text != "" && problemTextBox.Text != "" && hourComboBox.Text != "" && minComboBox.Text != "")
			{
				if (appointmentService.getAppointmentDateTimeAvailable(dateMakeAppointmentTextBox.Text, time))
				{
					if (appointmentService.makeAppointment(clientTextBox.Text, phoneTextBox.Text, dateMakeAppointmentTextBox.Text, time, carTextBox.Text, problemTextBox.Text, status))
					{
						MessageBox.Show("Appointment registered");
					}
					else
					{
						MessageBox.Show("Appointment not registered");
					}
				}
				else
				{
					MessageBox.Show("Date and time not available");
				}

			}
		}

		// BUTTON UPDATE STATUS APPOINTMENT
		private void updateStatusAppointmentButton_Click(object sender, EventArgs e)
		{
			if (appointmentService.updateAppointment(_dateUpdate, _timeUpdate, _statusUpdate))
			{
				//dateOneDayTextBox.Text = monthCalendarOneDay.SelectionStart.Date.ToString("yyyy-MM-dd");
				if (_dateUpdate != "")
				{
					List<Appointment> appointmentList = appointmentService.getAppointmentsForOneDay(_dateUpdate);

					DataTable table = ToDataTable(appointmentList);
					dataGridView2.DataSource = table;
				}
				MessageBox.Show("Updated successful");
			}
			else
			{
				MessageBox.Show("Not updated");
			}


		}

			// ---- CALENDARS ---- //
		// CALENDAR ONE DAY DATE CHANGED action
		private void monthCalendarOneDay_DateChanged(object sender, DateRangeEventArgs e)
		{
			dateOneDayTextBox.Text = monthCalendarOneDay.SelectionStart.Date.ToString("yyyy-MM-dd");
			if (dateOneDayTextBox.Text != "")
			{
				List<Appointment> appointmentList = appointmentService.getAppointmentsForOneDay(dateOneDayTextBox.Text);

				DataTable table = ToDataTable(appointmentList);
				dataGridView2.DataSource = table;
			}
		}

		// CALENDAR MAKE APPPOINTMENT CHANGED action
		private void monthCalendarMakeAppointment_DateChanged(object sender, DateRangeEventArgs e)
		{
			dateMakeAppointmentTextBox.Text = monthCalendarMakeAppointment.SelectionStart.Date.ToString("yyyy-MM-dd");
		}

			// ---- DATAGRIDVIEW ---- //
		// CELL CONTENT CLICKED DATAGRIDVIEW
		private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			_timeUpdate = dataGridView2.Rows[e.RowIndex].Cells[3].Value.ToString();
			_statusUpdate = dataGridView2.Rows[e.RowIndex].Cells[6].Value.ToString();
			dateOneDayTextBox.Text = monthCalendarOneDay.SelectionStart.Date.ToString("yyyy-MM-dd");
			_dateUpdate = dateOneDayTextBox.Text;
		}

	}
}
