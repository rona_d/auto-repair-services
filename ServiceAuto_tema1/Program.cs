﻿using System;
using System.Windows.Forms;
using ServiceAuto_tema1.GUI;
using ServiceAuto_tema1.BL;
using ServiceAuto_tema1.RepositoryL.Repositories;

namespace ServiceAuto_tema1
{
	static class Program
	{
		public static UserService USERSERVICE;
		public static AppointmentService APPOINTMENTSERVICE;
		
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			USERSERVICE = new UserService(new MSSQLUserRepository());
			APPOINTMENTSERVICE = new AppointmentService(new MSSQLAppointmentRepository());
			
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(LoginForm.getInstance());
		}
	}
}
