﻿namespace ServiceAuto_tema1.GUI
{
	partial class AdminForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ViewAll2DatesAppointmentsButton = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.buttonsPanel = new System.Windows.Forms.Panel();
			this.CreateServiceAgentButton = new System.Windows.Forms.Button();
			this.logOutButton = new System.Windows.Forms.Button();
			this.ViewClientAppointmentsButton = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
			this.viewClientAppointmentsPanel = new System.Windows.Forms.Panel();
			this.showClientAppointmentsButton = new System.Windows.Forms.Button();
			this.clientAppointmentsDataGrid = new System.Windows.Forms.DataGridView();
			this.clientNameTextBox = new System.Windows.Forms.TextBox();
			this.clientNameLabel = new System.Windows.Forms.Label();
			this.DateColumnClient = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.TimeColumnClient = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.CarColumnClient = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ProblemColumnClient = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.StatusColumnClient = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.PhoneColumnClient = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.viewAppointmentsBetween2DatesPanel = new System.Windows.Forms.Panel();
			this.date2TextBox = new System.Windows.Forms.TextBox();
			this.date1TextBox = new System.Windows.Forms.TextBox();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.monthCalendar2 = new System.Windows.Forms.MonthCalendar();
			this.DateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.TimeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ClientColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.PhoneColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.CarColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ProblemColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.StatusColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.createServiceAgentPanel = new System.Windows.Forms.Panel();
			this.label6 = new System.Windows.Forms.Label();
			this.createButton = new System.Windows.Forms.Button();
			this.nameTextBox = new System.Windows.Forms.TextBox();
			this.passwordTextBox = new System.Windows.Forms.TextBox();
			this.usernameTextBox = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.usernameLabel = new System.Windows.Forms.Label();
			this.buttonsPanel.SuspendLayout();
			this.viewClientAppointmentsPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.clientAppointmentsDataGrid)).BeginInit();
			this.viewAppointmentsBetween2DatesPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.createServiceAgentPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// ViewAll2DatesAppointmentsButton
			// 
			this.ViewAll2DatesAppointmentsButton.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.ViewAll2DatesAppointmentsButton.Font = new System.Drawing.Font("Lucida Sans", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ViewAll2DatesAppointmentsButton.Location = new System.Drawing.Point(21, 54);
			this.ViewAll2DatesAppointmentsButton.Name = "ViewAll2DatesAppointmentsButton";
			this.ViewAll2DatesAppointmentsButton.Size = new System.Drawing.Size(180, 54);
			this.ViewAll2DatesAppointmentsButton.TabIndex = 0;
			this.ViewAll2DatesAppointmentsButton.Text = "View all appointments between 2 dates";
			this.ViewAll2DatesAppointmentsButton.UseVisualStyleBackColor = false;
			this.ViewAll2DatesAppointmentsButton.Click += new System.EventHandler(this.ViewAll2DatesAppointmentsButton_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.BackColor = System.Drawing.Color.LightBlue;
			this.label1.Font = new System.Drawing.Font("Minion Pro SmBd", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(52, 44);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(197, 27);
			this.label1.TabIndex = 1;
			this.label1.Text = "WELCOME, ADMIN!";
			// 
			// buttonsPanel
			// 
			this.buttonsPanel.Controls.Add(this.CreateServiceAgentButton);
			this.buttonsPanel.Controls.Add(this.logOutButton);
			this.buttonsPanel.Controls.Add(this.ViewAll2DatesAppointmentsButton);
			this.buttonsPanel.Controls.Add(this.ViewClientAppointmentsButton);
			this.buttonsPanel.Location = new System.Drawing.Point(36, 117);
			this.buttonsPanel.Name = "buttonsPanel";
			this.buttonsPanel.Size = new System.Drawing.Size(235, 470);
			this.buttonsPanel.TabIndex = 2;
			// 
			// CreateServiceAgentButton
			// 
			this.CreateServiceAgentButton.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.CreateServiceAgentButton.Font = new System.Drawing.Font("Lucida Sans", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.CreateServiceAgentButton.Location = new System.Drawing.Point(21, 251);
			this.CreateServiceAgentButton.Name = "CreateServiceAgentButton";
			this.CreateServiceAgentButton.Size = new System.Drawing.Size(180, 63);
			this.CreateServiceAgentButton.TabIndex = 3;
			this.CreateServiceAgentButton.Text = "Create Service Agent account";
			this.CreateServiceAgentButton.UseVisualStyleBackColor = false;
			this.CreateServiceAgentButton.Click += new System.EventHandler(this.CreateServiceAgentButton_Click);
			// 
			// logOutButton
			// 
			this.logOutButton.BackColor = System.Drawing.Color.GhostWhite;
			this.logOutButton.Font = new System.Drawing.Font("Minion Pro SmBd", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.logOutButton.Location = new System.Drawing.Point(0, 413);
			this.logOutButton.Name = "logOutButton";
			this.logOutButton.Size = new System.Drawing.Size(235, 32);
			this.logOutButton.TabIndex = 4;
			this.logOutButton.Text = "Log Out";
			this.logOutButton.UseVisualStyleBackColor = false;
			this.logOutButton.Click += new System.EventHandler(this.logOutButton_Click);
			// 
			// ViewClientAppointmentsButton
			// 
			this.ViewClientAppointmentsButton.BackColor = System.Drawing.Color.GhostWhite;
			this.ViewClientAppointmentsButton.Font = new System.Drawing.Font("Lucida Sans", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ViewClientAppointmentsButton.Location = new System.Drawing.Point(21, 145);
			this.ViewClientAppointmentsButton.Name = "ViewClientAppointmentsButton";
			this.ViewClientAppointmentsButton.Size = new System.Drawing.Size(180, 60);
			this.ViewClientAppointmentsButton.TabIndex = 1;
			this.ViewClientAppointmentsButton.Text = "View client\'s appointments";
			this.ViewClientAppointmentsButton.UseVisualStyleBackColor = false;
			this.ViewClientAppointmentsButton.Click += new System.EventHandler(this.ViewClientAppointmentsButton_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Lucida Sans", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(191, 253);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(59, 17);
			this.label2.TabIndex = 0;
			this.label2.Text = "date 1";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Lucida Sans", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(581, 250);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(59, 17);
			this.label3.TabIndex = 2;
			this.label3.Text = "date 2";
			// 
			// monthCalendar1
			// 
			this.monthCalendar1.AllowDrop = true;
			this.monthCalendar1.Location = new System.Drawing.Point(194, 26);
			this.monthCalendar1.MaxSelectionCount = 1;
			this.monthCalendar1.Name = "monthCalendar1";
			this.monthCalendar1.TabIndex = 5;
			this.monthCalendar1.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateChanged);
			// 
			// viewClientAppointmentsPanel
			// 
			this.viewClientAppointmentsPanel.Controls.Add(this.showClientAppointmentsButton);
			this.viewClientAppointmentsPanel.Controls.Add(this.clientAppointmentsDataGrid);
			this.viewClientAppointmentsPanel.Controls.Add(this.clientNameTextBox);
			this.viewClientAppointmentsPanel.Controls.Add(this.clientNameLabel);
			this.viewClientAppointmentsPanel.Location = new System.Drawing.Point(293, 43);
			this.viewClientAppointmentsPanel.Name = "viewClientAppointmentsPanel";
			this.viewClientAppointmentsPanel.Size = new System.Drawing.Size(1061, 667);
			this.viewClientAppointmentsPanel.TabIndex = 6;
			// 
			// showClientAppointmentsButton
			// 
			this.showClientAppointmentsButton.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.showClientAppointmentsButton.Font = new System.Drawing.Font("Minion Pro Cond", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.showClientAppointmentsButton.Location = new System.Drawing.Point(182, 450);
			this.showClientAppointmentsButton.Name = "showClientAppointmentsButton";
			this.showClientAppointmentsButton.Size = new System.Drawing.Size(679, 36);
			this.showClientAppointmentsButton.TabIndex = 14;
			this.showClientAppointmentsButton.Text = "Show client\'s appointments";
			this.showClientAppointmentsButton.UseVisualStyleBackColor = false;
			this.showClientAppointmentsButton.Click += new System.EventHandler(this.showClientAppointmentsButton_Click);
			// 
			// clientAppointmentsDataGrid
			// 
			this.clientAppointmentsDataGrid.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
			this.clientAppointmentsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.clientAppointmentsDataGrid.Location = new System.Drawing.Point(17, 168);
			this.clientAppointmentsDataGrid.Name = "clientAppointmentsDataGrid";
			this.clientAppointmentsDataGrid.RowTemplate.Height = 24;
			this.clientAppointmentsDataGrid.Size = new System.Drawing.Size(1013, 230);
			this.clientAppointmentsDataGrid.TabIndex = 13;
			// 
			// clientNameTextBox
			// 
			this.clientNameTextBox.Location = new System.Drawing.Point(149, 115);
			this.clientNameTextBox.Name = "clientNameTextBox";
			this.clientNameTextBox.Size = new System.Drawing.Size(224, 22);
			this.clientNameTextBox.TabIndex = 12;
			// 
			// clientNameLabel
			// 
			this.clientNameLabel.AutoSize = true;
			this.clientNameLabel.Cursor = System.Windows.Forms.Cursors.Default;
			this.clientNameLabel.Font = new System.Drawing.Font("Lucida Sans", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.clientNameLabel.Location = new System.Drawing.Point(165, 95);
			this.clientNameLabel.Name = "clientNameLabel";
			this.clientNameLabel.Size = new System.Drawing.Size(172, 16);
			this.clientNameLabel.TabIndex = 11;
			this.clientNameLabel.Text = "INSERT CLIENT\'S NAME";
			// 
			// DateColumnClient
			// 
			this.DateColumnClient.Name = "DateColumnClient";
			// 
			// TimeColumnClient
			// 
			this.TimeColumnClient.Name = "TimeColumnClient";
			// 
			// CarColumnClient
			// 
			this.CarColumnClient.Name = "CarColumnClient";
			// 
			// ProblemColumnClient
			// 
			this.ProblemColumnClient.Name = "ProblemColumnClient";
			// 
			// StatusColumnClient
			// 
			this.StatusColumnClient.Name = "StatusColumnClient";
			// 
			// PhoneColumnClient
			// 
			this.PhoneColumnClient.Name = "PhoneColumnClient";
			// 
			// viewAppointmentsBetween2DatesPanel
			// 
			this.viewAppointmentsBetween2DatesPanel.Controls.Add(this.date2TextBox);
			this.viewAppointmentsBetween2DatesPanel.Controls.Add(this.date1TextBox);
			this.viewAppointmentsBetween2DatesPanel.Controls.Add(this.dataGridView1);
			this.viewAppointmentsBetween2DatesPanel.Controls.Add(this.monthCalendar2);
			this.viewAppointmentsBetween2DatesPanel.Controls.Add(this.label3);
			this.viewAppointmentsBetween2DatesPanel.Controls.Add(this.monthCalendar1);
			this.viewAppointmentsBetween2DatesPanel.Controls.Add(this.label2);
			this.viewAppointmentsBetween2DatesPanel.Location = new System.Drawing.Point(296, 26);
			this.viewAppointmentsBetween2DatesPanel.Name = "viewAppointmentsBetween2DatesPanel";
			this.viewAppointmentsBetween2DatesPanel.Size = new System.Drawing.Size(1058, 684);
			this.viewAppointmentsBetween2DatesPanel.TabIndex = 10;
			// 
			// date2TextBox
			// 
			this.date2TextBox.Location = new System.Drawing.Point(646, 251);
			this.date2TextBox.Name = "date2TextBox";
			this.date2TextBox.Size = new System.Drawing.Size(185, 22);
			this.date2TextBox.TabIndex = 14;
			// 
			// date1TextBox
			// 
			this.date1TextBox.Location = new System.Drawing.Point(256, 251);
			this.date1TextBox.Name = "date1TextBox";
			this.date1TextBox.Size = new System.Drawing.Size(185, 22);
			this.date1TextBox.TabIndex = 13;
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowUserToOrderColumns = true;
			this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Location = new System.Drawing.Point(14, 312);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.RowTemplate.Height = 24;
			this.dataGridView1.Size = new System.Drawing.Size(1031, 340);
			this.dataGridView1.TabIndex = 9;
			// 
			// monthCalendar2
			// 
			this.monthCalendar2.Location = new System.Drawing.Point(586, 25);
			this.monthCalendar2.MaxSelectionCount = 1;
			this.monthCalendar2.Name = "monthCalendar2";
			this.monthCalendar2.TabIndex = 6;
			this.monthCalendar2.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar2_DateChanged);
			// 
			// DateColumn
			// 
			this.DateColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
			this.DateColumn.HeaderText = "Date";
			this.DateColumn.Name = "DateColumn";
			this.DateColumn.ReadOnly = true;
			// 
			// TimeColumn
			// 
			this.TimeColumn.HeaderText = "Time";
			this.TimeColumn.Name = "TimeColumn";
			this.TimeColumn.ReadOnly = true;
			// 
			// ClientColumn
			// 
			this.ClientColumn.HeaderText = "Client";
			this.ClientColumn.Name = "ClientColumn";
			this.ClientColumn.ReadOnly = true;
			// 
			// PhoneColumn
			// 
			this.PhoneColumn.HeaderText = "Phone";
			this.PhoneColumn.Name = "PhoneColumn";
			this.PhoneColumn.ReadOnly = true;
			// 
			// CarColumn
			// 
			this.CarColumn.HeaderText = "Car";
			this.CarColumn.Name = "CarColumn";
			this.CarColumn.ReadOnly = true;
			// 
			// ProblemColumn
			// 
			this.ProblemColumn.HeaderText = "Problem";
			this.ProblemColumn.Name = "ProblemColumn";
			this.ProblemColumn.ReadOnly = true;
			// 
			// StatusColumn
			// 
			this.StatusColumn.HeaderText = "Status";
			this.StatusColumn.Name = "StatusColumn";
			this.StatusColumn.ReadOnly = true;
			this.StatusColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.StatusColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			// 
			// createServiceAgentPanel
			// 
			this.createServiceAgentPanel.Controls.Add(this.label6);
			this.createServiceAgentPanel.Controls.Add(this.createButton);
			this.createServiceAgentPanel.Controls.Add(this.nameTextBox);
			this.createServiceAgentPanel.Controls.Add(this.passwordTextBox);
			this.createServiceAgentPanel.Controls.Add(this.usernameTextBox);
			this.createServiceAgentPanel.Controls.Add(this.label5);
			this.createServiceAgentPanel.Controls.Add(this.usernameLabel);
			this.createServiceAgentPanel.Location = new System.Drawing.Point(293, 26);
			this.createServiceAgentPanel.Name = "createServiceAgentPanel";
			this.createServiceAgentPanel.Size = new System.Drawing.Size(1061, 684);
			this.createServiceAgentPanel.TabIndex = 7;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Lucida Sans", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(394, 355);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(55, 19);
			this.label6.TabIndex = 6;
			this.label6.Text = "name";
			// 
			// createButton
			// 
			this.createButton.BackColor = System.Drawing.SystemColors.Window;
			this.createButton.Font = new System.Drawing.Font("Minion Pro Cond", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.createButton.Location = new System.Drawing.Point(149, 448);
			this.createButton.Name = "createButton";
			this.createButton.Size = new System.Drawing.Size(837, 37);
			this.createButton.TabIndex = 5;
			this.createButton.Text = "Create new service agent";
			this.createButton.UseVisualStyleBackColor = false;
			this.createButton.Click += new System.EventHandler(this.createButton_Click);
			// 
			// nameTextBox
			// 
			this.nameTextBox.Location = new System.Drawing.Point(499, 352);
			this.nameTextBox.Name = "nameTextBox";
			this.nameTextBox.Size = new System.Drawing.Size(224, 22);
			this.nameTextBox.TabIndex = 4;
			// 
			// passwordTextBox
			// 
			this.passwordTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.passwordTextBox.Location = new System.Drawing.Point(499, 248);
			this.passwordTextBox.Name = "passwordTextBox";
			this.passwordTextBox.PasswordChar = '*';
			this.passwordTextBox.Size = new System.Drawing.Size(224, 22);
			this.passwordTextBox.TabIndex = 3;
			// 
			// usernameTextBox
			// 
			this.usernameTextBox.Location = new System.Drawing.Point(499, 145);
			this.usernameTextBox.Name = "usernameTextBox";
			this.usernameTextBox.Size = new System.Drawing.Size(224, 22);
			this.usernameTextBox.TabIndex = 2;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Lucida Sans", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(380, 248);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(94, 19);
			this.label5.TabIndex = 1;
			this.label5.Text = "password";
			// 
			// usernameLabel
			// 
			this.usernameLabel.AutoSize = true;
			this.usernameLabel.Font = new System.Drawing.Font("Lucida Sans", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.usernameLabel.Location = new System.Drawing.Point(377, 145);
			this.usernameLabel.Name = "usernameLabel";
			this.usernameLabel.Size = new System.Drawing.Size(91, 19);
			this.usernameLabel.TabIndex = 0;
			this.usernameLabel.Text = "username";
			// 
			// AdminForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.LightBlue;
			this.ClientSize = new System.Drawing.Size(1377, 773);
			this.Controls.Add(this.buttonsPanel);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.viewAppointmentsBetween2DatesPanel);
			this.Controls.Add(this.viewClientAppointmentsPanel);
			this.Controls.Add(this.createServiceAgentPanel);
			this.Name = "AdminForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "AdminForm";
			this.buttonsPanel.ResumeLayout(false);
			this.viewClientAppointmentsPanel.ResumeLayout(false);
			this.viewClientAppointmentsPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.clientAppointmentsDataGrid)).EndInit();
			this.viewAppointmentsBetween2DatesPanel.ResumeLayout(false);
			this.viewAppointmentsBetween2DatesPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.createServiceAgentPanel.ResumeLayout(false);
			this.createServiceAgentPanel.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button ViewAll2DatesAppointmentsButton;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel buttonsPanel;
		private System.Windows.Forms.Button CreateServiceAgentButton;
		private System.Windows.Forms.Button ViewClientAppointmentsButton;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.MonthCalendar monthCalendar1;
		private System.Windows.Forms.Panel viewClientAppointmentsPanel;
		private System.Windows.Forms.MonthCalendar monthCalendar2;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.Panel viewAppointmentsBetween2DatesPanel;
		private System.Windows.Forms.DataGridViewTextBoxColumn DateColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn TimeColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn ClientColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn PhoneColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn CarColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn ProblemColumn;
		private System.Windows.Forms.DataGridViewCheckBoxColumn StatusColumn;
		private System.Windows.Forms.Button showClientAppointmentsButton;
		private System.Windows.Forms.DataGridView clientAppointmentsDataGrid;
		private System.Windows.Forms.DataGridViewTextBoxColumn DateColumnClient;
		private System.Windows.Forms.DataGridViewTextBoxColumn TimeColumnClient;
		private System.Windows.Forms.DataGridViewTextBoxColumn CarColumnClient;
		private System.Windows.Forms.DataGridViewTextBoxColumn ProblemColumnClient;
		private System.Windows.Forms.DataGridViewCheckBoxColumn StatusColumnClient;
		private System.Windows.Forms.DataGridViewTextBoxColumn PhoneColumnClient;
		private System.Windows.Forms.TextBox clientNameTextBox;
		private System.Windows.Forms.Label clientNameLabel;
		private System.Windows.Forms.Panel createServiceAgentPanel;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Button createButton;
		private System.Windows.Forms.TextBox nameTextBox;
		private System.Windows.Forms.TextBox passwordTextBox;
		private System.Windows.Forms.TextBox usernameTextBox;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label usernameLabel;
		private System.Windows.Forms.Button logOutButton;
		private System.Windows.Forms.TextBox date2TextBox;
		private System.Windows.Forms.TextBox date1TextBox;
	}
}